// Repitition Control Structures

// While Loop
/* Syntax:
	while(condition){
		statements/s;
	}
*/

let count = 5;

while(count !==0){
	console.log('While: ' + count);
	count--;
}

console.log('Displays numbers 1-10');
count = 1;

while(count < 11){
	console.log('While: ' + count);
	count++;
}

// do while loop

console.log('Do While Loop')

/*Syntax
	do{
		statement;
	}while (condition);
*/

let number = Number(prompt('Give a number'));

do{
	console.log('Do While: ' + number);
	number += 1;
}while (number < 10);

// Create a new variable to be used in displaying even numbers from 2-10  using while loop

console.log('Even numbers');

let even = 2;

do{
	console.log('Even Numbers: ' + even);
	even += 2;
}while(even <= 10);

// for loop
/*syntax
	for(initialization;condition;stepExpression){
		statement;
	}
*/




console.log('For Loop')



for(let countB = 0;countB <= 20; countB++){
	console.log(countB);
}

console.log('even for loop');
let evenB = 2
for(let counter = 1; counter <= 5; counter++){
	console.log('Even: ' + evenB);
	evenB += 2;
}

let myString = 'Alex';
// .length property is used to count the characters in a string
console.log(myString.length);

console.log(myString[0]);
console.log(myString[3]);

for(let x = 0; x < myString.length; x++){
	console.log(myString[x])
}


// print out letters individually but will print 3 instead of the vowels

let myName = "AlEx";

for(let i = 0; i < myName.length; i++){
	if(
		myName[i].toLowerCase() == 'a' ||
		myName[i].toLowerCase() == 'e' ||
		myName[i].toLowerCase() == 'i' ||
		myName[i].toLowerCase() == 'o' ||
		myName[i].toLowerCase() == 'u'
	){
		console.log(3);
	}
	else{
		console.log(myName[i]);
	}
}

// Continue and Break Statements

for(let count = 0; count <= 20; count++){
	// if remainder is equal to 0, tells the code to continue to iterate
	if(count % 2 == 0){
		continue;
	}
	console.log('Continue and Break: ' + count)
	// if the current value of count is greater than 10, tells thee code to stop the loop
	if(count > 10){
		break;
	}
}

let name = 'alexandro';

	/*
		name = 9
		i = 0
	*/

	
for(let i = 0; i < name.length; i++){
	console.log(name[i]);

	if(name[i].toLowerCase() === 'a'){
		console.log('Continue to the next Iteration');
		continue;
	}

	if(name[i] ==='d'){
		break;
	}
}